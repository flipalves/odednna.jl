abstract type AbstractEDNNAgent <: AbstractAgent end

# Default Neural Network Agent with an Entropic Learning Dynamics
# for Opinion and Distrust
mutable struct ODAgent{K,N} <: AbstractEDNNAgent
    id::Int
    pos::Int
    w::Vector{Float64}
    C::Matrix{Float64}
    m::Vector{Float64}
    V::Vector{Float64}    
end

# EDNNA agent for Opinion and Distrust with an
# Isotropic Homogeneous C baked into the ED constraints
mutable struct ODAgentIHC{K,N} <: AbstractEDNNAgent
    id::Int
    pos::Int
    w::Vector{Float64}
    C::Float64
    m::Vector{Float64}
    V::Vector{Float64}
end

"""Initializes an agents with id i and state variables
w,C for opinion behavior and m,V for distrust behavior
"""
function odagent(i,w::Tw,C::TC,m::Tm,V::TV) where {Tw<:AbstractVector,TC<:AbstractMatrix,Tm<:AbstractVector,TV<:AbstractVector}
    K,N = size(w,1),size(m,1)
    return ODAgent{K,N}(i,i,w,C,m,V)
end

function odagent(i,w::Tw,C::TC,m::Tm,V::TV) where {Tw<:AbstractVector,TC<:Real,Tm<:AbstractVector,TV<:AbstractVector}
    K,N = size(w,1),size(m,1)
    return ODAgentIHC{K,N}(i,i,w,C,m,V)
end

_C_identity(A::Type{<:ODAgent}) = ((K,_)=A.parameters; Matrix{Float64}(I,(K,K)))
_C_identity(A::Type{<:ODAgentIHC}) = 1.0
_one_cold(n::Int, i::Int) = (v=ones(n); v[i]=0.0; v) 

function Base.rand(rng::AbstractRNG, A::Type{<:AbstractEDNNAgent})
    K, N = A.parameters
    agents = [odagent(i, randn(rng, K), _C_identity(A), _one_cold(N,i).*randn(rng, N), _one_cold(N,i))
              for i in 1:N]
    return agents
end

Base.rand(A::Type{<:AbstractEDNNAgent}) = rand(Random.GLOBAL_RNG, A)

function scale_agent!(a::A; w=1.0, m=1.0, C=1.0, V=1.0) where {A<:AbstractEDNNAgent}
    a.w *= w
    a.C *= C
    a.m *= m
    a.V *= V
    return a
end

function shift_agent!(a::A; w::Union{Float64,<:AbstractVector}=0.0, m::Union{Float64,<:AbstractVector}=0.0) where {A<:AbstractEDNNAgent}
    a.w .+= w
    a.m .+= m
    return a
end

function LinearAlgebra.normalize!(a::A; w=true, C=false, m=true, V=false) where {A<:AbstractEDNNAgent}
    w && normalize!(a.w)
    m && normalize!(a.m)
    V && normalize!(a.V)
    if C
        a.C = a.C./norm(a.C)
    end
    return a
end

# Default model constructors
function odsociety(;
                    agents::Vector{A},
                    issues::Vector{<:AbstractVector},
                    rng,
		    topology,
		    scheduler=Schedulers.randomly,
		    properties...
                ) where {A<:AbstractEDNNAgent}
    K, N = A.parameters
    P = length(issues)
    properties = merge(@dict(K,N,issues), properties)
    # rng = Random.MersenneTwister(seed)

    society = ABM(
                A,
                GraphSpace(topology);
                scheduler,
                properties, 
                rng
            )

    add_agent_pos!.(agents,[society])

    return society
end

# Number of "degrees of freedom" for an agent and a 
# society of such agents
complexity(::Union{Type{ODAgent{K,N}},Type{ODAgentIHC{K,N}}}) where {K,N} = N*(K + N-1)

complexity(a::A) where {A<:AbstractEDNNAgent} = complexity(A)

complexity(::Type{ABM{<:Agents.AbstractSpace,A}}) where {A<:AbstractAgent} = complexity(A)

complexity(m::ABM{<:Agents.AbstractSpace,A}) where {A<:AbstractAgent} = complexity(A)

complexity(K::Int,N::Int) = complexity(ODAgent{K,N})


# Theory:
# Z - the evidence
# F - the modulation function
# γ - the internal representation uncertainty
G(x) = pdf(Normal(0,1),x)

Φ(x) = cdf(Normal(0,1),x)

Z(x,y) = (ϕ=Φ(x); ψ=Φ(y); ϕ + ψ - 2ϕ*ψ)

F(x,y) = (f=(1-2Φ(y))G(x)/Z(x,y); (weight=f,uncertainty=-(f+x)f))

function modulations(hσ_γ,mer_λ)
	ϕ=Φ(hσ_γ)
	ψ=Φ(mer_λ)
	z = ϕ + ψ - 2ϕ*ψ
	fw = (1-2ψ)G(hσ_γ)/z
	fC = -(fw + hσ_γ)fw
	fm = (1-2ϕ)G(mer_λ)/z
	fV = -(fm + mer_λ)fm
	(;fw,fC,fm,fV)
end

γ(z::Float64) = sqrt(1 + z)
γ0(z::Float64) = sqrt(z)

function _update_C!(a::A,x,fCσ_γ²) where {A<:ODAgent}
	a.C .+= fCσ_γ².*(a.C*(x*x')*a.C)	
	return a
end

function _update_C!(a::A,x,fCσ_γ²) where {A<:ODAgentIHC}
	a.C += dot(x,x)*fCσ_γ²*a.C^2
	return a
end

# A receiving agent `r` reacts to an opiniofor nated issue 
# from `e` through the surprise function `F`
# resulting in a set of updates `Δ = (Δw,ΔC,Δm,ΔV)`
# function backward!(a_r::A,c::C) where {A<:AbstractEDNNAgent,C<:ODContext}
function entropic_dynamics!(a_r::A,society) where {A<:AbstractEDNNAgent}
	# context
	@views x = rand(society.rng, society.issues)
	e = rand(society.rng, nearby_ids(a_r,society))
	σ = sign(society[e].w ⋅ x)
	
	# learning
	σ_γC = σ/γ(dot(x, a_r.C, x))
	hσ_γC = dot(a_r.w, σ_γC, x)
    
	_γV =  1.0/γ(a_r.V[e])
	mer_γV = a_r.m[e] * _γV
	Ver_γV = a_r.V[e] * _γV
    	Ver²_γV² = Ver_γV^2
    
   	fw, fC, fm, fV = modulations(hσ_γC,mer_γV)
    
	# a_r.w += (a_r.C*x).*(fw*σ_γC)
	mul!(a_r.w,a_r.C,x,fw*σ_γC,1.0)
	_update_C!(a_r,x,fC*σ_γC^2)
	a_r.m[e] += fm*Ver_γV
	a_r.V[e] += fV*Ver²_γV²
	
    return a_r
end

function entropic_dynamics_no_issue_noise!(a_r,society)
	# context
	@views x = rand(society.rng, society.issues)
	e = rand(society.rng, nearby_ids(a_r,society))
	σ = sign(society[e].w ⋅ x)
	
	# learning
	σ_γC = σ/γ0(dot(x, a_r.C, x))
	hσ_γC = dot(a_r.w, σ_γC, x)
    
	_γV =  1.0/γ(a_r.V[e])
	mer_γV = a_r.m[e] * _γV
	Ver_γV = a_r.V[e] * _γV
    	Ver²_γV² = Ver_γV^2
    
   	fw, fC, fm, fV = modulations(hσ_γC,mer_γV)
    
	# a_r.w += (a_r.C*x).*(fw*σ_γC)
	mul!(a_r.w,a_r.C,x,fw*σ_γC,1.0)
	_update_C!(a_r,x,fC*σ_γC^2)
	a_r.m[e] += fm*Ver_γV
	a_r.V[e] += fV*Ver²_γV²
	
    return a_r
end

function entropic_dynamics_no_issue_noise_normalized_weights!(a_r,society)
	# context
	@views x = rand(society.rng, society.issues)
	e = rand(society.rng, nearby_ids(a_r,society))
	σ = sign(society[e].w ⋅ x)
	
	# learning
	σ_γC = σ/γ0(dot(x, a_r.C, x))
	hσ_γC = dot(a_r.w, σ_γC, x)
    
	_γV =  1.0/γ(a_r.V[e])
	mer_γV = a_r.m[e] * _γV
	Ver_γV = a_r.V[e] * _γV
    	Ver²_γV² = Ver_γV^2
    
   	fw, fC, fm, fV = modulations(hσ_γC,mer_γV)
    
	# a_r.w += (a_r.C*x).*(fw*σ_γC)
	mul!(a_r.w,a_r.C,x,fw*σ_γC,1.0)
	_update_C!(a_r,x,fC*σ_γC^2)
	a_r.m[e] += fm*Ver_γV
	a_r.V[e] += fV*Ver²_γV²

	normalize!(a_r; w=true, m=false)
	scale_agent!(a_r; w=sqrt(1+norm(a_r.C)))
	
    return a_r
end

function entropic_dynamics_no_issue_noise_normalized_weights_to_1!(a_r,society)
	# context
	@views x = rand(society.rng, society.issues)
	e = rand(society.rng, nearby_ids(a_r,society))
	σ = sign(society[e].w ⋅ x)
	
	# learning
	σ_γC = σ/γ0(dot(x, a_r.C, x))
	hσ_γC = dot(a_r.w, σ_γC, x)
    
	_γV =  1.0/γ(a_r.V[e])
	mer_γV = a_r.m[e] * _γV
	Ver_γV = a_r.V[e] * _γV
    	Ver²_γV² = Ver_γV^2
    
   	fw, fC, fm, fV = modulations(hσ_γC,mer_γV)
    
	# a_r.w += (a_r.C*x).*(fw*σ_γC)
	mul!(a_r.w,a_r.C,x,fw*σ_γC,1.0)
	_update_C!(a_r,x,fC*σ_γC^2)
	a_r.m[e] += fm*Ver_γV
	a_r.V[e] += fV*Ver²_γV²

	normalize!(a_r; w=true, m=false)
	
    return a_r
end
