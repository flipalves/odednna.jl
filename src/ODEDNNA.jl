module ODEDNNA

using Agents
using Agents: AbstractSpace
using Distributions
using BSON
using LightGraphs
using LinearAlgebra
using Random

using DrWatson: produce_or_load, datadir,
                @dict, @ntuple, ntuple2dict, struct2dict

include("./opinion-distrust-model.jl")
include("./simulation-setup.jl")

# from opinion-distrust-model.jl
export AbstractEDNNAgent, ODAgent, ODAgentIHC
export odsociety, odagent, scale_agent!, shift_agent!, entropic_dynamics!, complexity

# from simulator.jl
export SimulationSetup

end # module