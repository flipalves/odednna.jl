Base.@kwdef struct SimulationSetup <: Function
    model_init::Function=odsociety
    agent_step!::Function=entropic_dynamics!
    model_step!::Function=dummystep
    adata::Any=nothing
    when::Union{Bool,Function}=true
    mdata::Any=nothing
    when_model::Union{Bool,Function}=true
    steps::Int=1
    seed::UInt8=rand(UInt8)
    parallel::Bool=false
    prefix::String=""
    suffix::String="bson"
    path::String=datadir("simulations")
end

function Base.getindex(s::SimulationSetup;kw...)
    d = struct2dict(s)
    merge!(d,kw)
    return SimulationSetup(;d...)
end

function (s::SimulationSetup)(parameters::Union{Dict,<:NamedTuple})
    if parameters isa NamedTuple
        parameters = ntuple2dict(parameters)
    end
    if any([isa(v,Array) for v in values(parameters)])
        adf,mdf = paramscan(
                        parameters, s.model_init;
                        agent_step! = s.agent_step!,
                        n=s.steps,
                        parallel=s.parallel,
                        adata=s.adata,when=s.when,
                        mdata=s.mdata,when_model=s.when_model,
                        obtainer=copy #,progress=true
                    )

        return Dict(:adata=>adf,:mdata=>mdf)
    else
        society = s.model_init(; parameters...)

        adf,mdf = run!(
                        society,s.agent_step!,s.model_step!,s.steps;
                        parallel=s.parallel,
                        adata=s.adata,when=s.when,
                        mdata=s.mdata,when_model=s.when_model,
                        obtainer=copy
                    )

        return Dict(:adata=>adf,:mdata=>mdf)
    end 
end

function (s::SimulationSetup)(; force=false, parameters...)
    data,fname = produce_or_load(
                        s.path,Dict(parameters),s;
                        prefix=s.prefix,suffix=s.suffix,
                        tag=false,force
                    )
    return data,fname
end
